﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMove : MonoBehaviour 
{	
	public float speed = 1;
	public float time = 20.0f;


	void Start()
	{
		Destroy(gameObject, time);
	}

	void Update()
	{
		transform.position -= Vector3.right * speed * Time.deltaTime;
	}
}
