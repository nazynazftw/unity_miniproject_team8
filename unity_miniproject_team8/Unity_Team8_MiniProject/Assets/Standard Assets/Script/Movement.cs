﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {
		
	public float speed;                //Floating point variable to store the player's movement speed.

	private Rigidbody2D rb2d;        //Store a reference to the Rigidbody2D component required to use 2D Physics.
	public Transform player;
	public float Force = 5f;
	public bool isJumping = false;
	[SerializeField]
	float DBLJumpTimer;
	[SerializeField]
	bool isDBLJumping;

	// Use this for initialization
	void Start()
	{
		//Get and store a reference to the Rigidbody2D component so that we can access it.
		rb2d = GetComponent<Rigidbody2D> ();
	}

	//FixedUpdate is called at a fixed interval and is independent of frame rate. Put physics code here.
	void FixedUpdate()
	{
		//Store the current horizontal input in the float moveHorizontal.
		float moveHorizontal = Input.GetAxis ("Horizontal");

		//Store the current vertical input in the float moveVertical.
		float moveVertical = Input.GetAxis ("Vertical");

		//Use the two store floats to create a new Vector2 variable movement.
		Vector2 movement = new Vector2 (moveHorizontal, moveVertical);

		//Call the AddForce function of our Rigidbody2D rb2d supplying movement multiplied by speed to move our player.
		rb2d.AddForce (movement * speed);

		if (isDBLJumping)
		{
			DBLJumpTimer += Time.deltaTime;
			if (DBLJumpTimer > 2f)
			{
				isDBLJumping = false;
				DBLJumpTimer = 0f;
			}
		}

		if (!isJumping && Input.GetKeyDown(KeyCode.W)) // Allow Jump Once.
		{
			rb2d.AddForce(Vector2.up * Force, ForceMode2D.Impulse);
			isDBLJumping = true;
		}
		else if (isDBLJumping && Input.GetKeyDown(KeyCode.W)) // Allow Double Jump.
		{
			rb2d.AddForce(Vector2.up * (Force / 1.5f), ForceMode2D.Impulse);
			if (rb2d.velocity.y < -2f)
			{
				rb2d.AddForce(Vector2.up * Force, ForceMode2D.Impulse);
			}
			isDBLJumping = false;
			DBLJumpTimer = 0f;
		}
	}
}
