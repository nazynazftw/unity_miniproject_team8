﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public void Levels()
    {
        Debug.Log("Choose Level");
        SceneManager.LoadScene("LevelSelect");
    }

    public void Instructions()
    {
        Debug.Log("Instructions");
        SceneManager.LoadScene("Instructions");
    }

	public void Back()
	{
		Debug.Log("Menu");
		SceneManager.LoadScene("Menu");
	}

    public void Quit()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
