﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public float speed = 5;
    public float time = 10.0f;


    void Start()
    {
        Destroy(gameObject, time);
    }

    void Update()
    {
		transform.position -= Vector3.right * speed * Time.deltaTime;
    }
}
