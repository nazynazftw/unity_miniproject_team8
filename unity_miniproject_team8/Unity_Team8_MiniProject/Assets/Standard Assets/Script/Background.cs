﻿using UnityEngine;

public class Background : MonoBehaviour 
{
	public GameObject prefab;
	public float delay = 0.5f;

	void Start() {
		InvokeRepeating("Spawn", 0f, delay);
	}

	void Spawn() {
		Vector3 position = new Vector3(45f, Random.Range(-5.5f, -1.5f), 10f);
		Instantiate(prefab, position, Quaternion.identity);
	}
}
