﻿using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject prefab;
	public float delay = Random.Range(0.2f, 5.0f);

    void Start() {
		InvokeRepeating("Spawn", Random.Range(0.1f, 5f), delay);
    }

    void Spawn() {
		Vector3 position = new Vector3(45f, Random.Range(1.5f, 5.5f), -0.85f);
        Instantiate(prefab, position, Quaternion.identity);
    }
}